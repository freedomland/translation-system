Translate = require("translations/main")

function tr(str)
	local ret = Translate[str]
	
	if ret == nil then	
		return str
	else
		return ret
	end
end
