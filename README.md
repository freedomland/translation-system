# Translation System

Easy translate any of the plugin.

### How to install

- Put translate.lua, tr_items.lua, tr_cells.lua and translations in CoreScripts/scripts

### Translation file format

Translation file is a basic file. For example:

```bash
"Some english string" = "Строка на английском"
"More untranslate string" = "Ещё одна не переведённая строка"
```

### How to use

Create translate file and place it to CoreScripts/scripts/translations. "Cook" the translations, run cook.sh in translations directory. That's it.

Wrap any string you want to translate in tr(), for example:

```lua
print(tr("Some english string"))
```

Return "Строка на английском" from previous example.

```lua
print(tr_cells("Tel Fyr"))
```

Return "Тель Фир". If there is no Tel Fyr it will return original string.

```lua
print(tr_items("writ_bero"))
```

Return "Приказ на Благородную Казнь". If there is no writ_bero it will return original string.
